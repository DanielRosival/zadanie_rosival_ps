#!/usr/bin/env python3
import socket
import sys
import re
import os
import signal

dictList = []
KEYS = []
pattern = r'([^\=]*)="(.[^\"]*.)"'
zoznam = open("zoznam.txt","r")
zoznam = zoznam.readlines()
zoznam[:] = [s.strip() for s in zoznam]
for line in zoznam:
	d = {}
	for m in re.finditer(pattern,line):
		KEY = m.group(1)
		VALUE = m.group(2)
		d[KEY.strip()] = VALUE.strip()
	dictList.append(d)
for k in dictList[0]:
	KEYS.append(k)
pocetRiadkov = len(zoznam)

def keys(f, zoznam, pocetRiadkov, userInput, KEYS, userKEYS, dictList):
	f.write(",".join(KEYS))
	f.write("\n")
	f.write('100 OK\n')
	return userKEYS

def length(f, zoznam, pocetRiadkov, userInput, KEYS, userKEYS, dictList):
	f.write('100 OK\n')
	f.write('%d\n' % pocetRiadkov)
	return userKEYS

def read(f, zoznam, pocetRiadkov, userInput, KEYS, userKEYS, dictList):
	if int(userInput.group(1)) < 0:
		f.write('201 Bad index\n')
		return userKEYS
	if int(userInput.group(1)) > (pocetRiadkov - 1):
		f.write('200 Index too big\n')
		return userKEYS
	if userKEYS:
		for userKey in userKEYS:
			value = dictList[int(userInput.group(1)) - 1][userKey]
			f.write('%s ' % value)
	else:
		for key in KEYS:
			value = dictList[int(userInput.group(1)) - 1][key]
			f.write('%s ' % value)
	f.write('\n')
	f.write('100 OK\n')
	return userKEYS

def select(f, zoznam, pocetRiadkov, userInput, KEYS, userKEYS, dictList):
	userInput = userInput.group(1)
	userInput = userInput.split(",")
	for k in KEYS:
		if any(k in s for s in userInput):
			validKey = True
	if not validKey:
		f.write('203 Unknown key\n')
		return
	return userInput

def reset(f, zoznam, pocetRiadkov, userInput, KEYS, userKEYS, dictList):
	return []


s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
s.bind(('',9999))
s.listen(5)
signal.signal(signal.SIGCHLD, signal.SIG_IGN)

request_table = {
	r"LENGTH": length,
	r"READ (-?\d+)": read,
	r"KEYS": keys,
	r"SELECT (.+)": select,
	r"RESET": reset
}

userKEYS = []

while True:
	cs,addr=s.accept()
	if os.fork() == 0:
		s.close()
		f=cs.makefile("rw", encoding = "utf-8")
		version_request=f.readline()
		version_request=version_request.strip()
		if re.match('VERSION \d+',version_request):
			f.write('100 OK\n')
			f.write('%s\n' % version_request.split( )[1])
			f.flush()
		else:
			f.write('203 Need version\n')
			cs.close()
			f.close()
			sys.exit(0)
			f.flush()
		while True:
			request = f.readline()
			if not request:
				f.close()
				cs.close()
				sys.exit(0)
			request = request.rstrip()
			for regex in request_table:
				m = re.match(regex,request)
				if m:
					userKEYS = request_table[regex](f, zoznam, pocetRiadkov, m, KEYS, userKEYS, dictList)
					f.flush()
					break
			else:
				f.write('202 Unknown command\n')
				f.flush()









